/*
  UNALZ : read and extract module for ALZ format.

  LICENSE (zlib License)
  Copyright (C) 2010-2018 portmasterATbsdforge.com : https://BSDforge.com
  Copyright (C) 2004-2009 kippler@gmail.com

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

*/

#ifndef _UNALZ_H_
#define _UNALZ_H_

#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

#ifndef INT64
#ifdef _WIN32
#	define INT64 __int64
#else
#	define INT64 long long
#endif
#endif

#ifndef UINT64
#ifdef _WIN32
#	define UINT64 unsigned __int64
#else
#	define UINT64 unsigned long long
#endif
#endif

#ifndef UINT32
	typedef unsigned int		UINT32;
#endif

#ifndef UINT16
	typedef unsigned short		UINT16;
#endif

#ifndef SHORT
	typedef short SHORT;
#endif
#ifndef BYTE
	typedef unsigned char       BYTE;
#endif
#ifndef CHAR
	typedef char CHAR;
#endif
#ifndef BYTE
	typedef unsigned char BYTE;
#endif
#ifndef UINT
	typedef unsigned int UINT;
#endif
#ifndef LONG
	typedef long LONG;
#endif
#ifndef BOOL
#	ifndef BOOL_DEFINED		// BOOL DEFINE BOOL_DEFINED ¦ define
	typedef int BOOL;
#	endif
#endif
#ifndef FALSE
#	define FALSE               0
#endif
#ifndef TRUE
#	define TRUE                1
#endif
#ifndef HANDLE
#	ifdef _WIN32
	typedef void *HANDLE;
#	else
	typedef FILE *HANDLE;
#	endif
#endif
#ifndef ASSERT
#	include <assert.h>
//#	define ASSERT(x) assert(x)
#	define ASSERT(x) {printf("unalz assert at file:%s line:%d\n", __FILE__, __LINE__);}
#endif

namespace UNALZ
{

#ifdef _WIN32
#	pragma pack(push, UNALZ, 1)			// structure packing 
#else
#	pragma pack(1)
#endif

static const char UNALZ_VERSION[]   = "CUnAlz0.66";
static const char UNALZ_COPYRIGHT[] = "Copyright(C) 2010-2018 by portmasterATbsdforge.com ( https://BSDforge.com ) ";

enum		{ALZ_ENCR_HEADER_LEN=12}; // xf86
//
struct SAlzHeader
{
	UINT32	unknown;			// ??
};

/*
union _UGeneralPurposeBitFlag			// zip
{
	SHORT	data;
	struct 
	{
		BYTE bit0 : 1;
		BYTE bit1 : 1;
		BYTE bit2 : 1;
		BYTE bit3 : 1;
		BYTE bit4 : 1;
		BYTE bit5 : 1;
	};
};
*/

enum COMPRESSION_METHOD					//
{
	COMP_NOCOMP = 0,
	COMP_BZIP2 = 1,
	COMP_DEFLATE = 2,
	COMP_UNKNOWN = 3,					// unknown!
};

enum ALZ_FILE_ATTRIBUTE
{
	ALZ_FILEATTR_READONLY	= 0x1,
	ALZ_FILEATTR_HIDDEN		= 0x2,
	ALZ_FILEATTR_DIRECTORY	= 0x10,
	ALZ_FILEATTR_FILE		= 0x20,			
};

enum ALZ_FILE_DESCRIPTOR
{
	ALZ_FILE_DESCRIPTOR_ENCRYPTED			= 0x01,		//
	ALZ_FILE_DESCRIPTOR_FILESIZEFIELD_1BYTE = 0x10,		//
	ALZ_FILE_DESCRIPTOR_FILESIZEFIELD_2BYTE = 0x20,
	ALZ_FILE_DESCRIPTOR_FILESIZEFIELD_4BYTE = 0x40,
	ALZ_FILE_DESCRIPTOR_FILESIZEFIELD_8BYTE = 0x80,
};

struct _SAlzLocalFileHeaderHead			//
{
	SHORT	fileNameLength;
	BYTE    fileAttribute;			    // from http://www.zap.pe.kr, enum FILE_ATTRIBUE Âü°í
	UINT32  fileTimeDate;				// dos file time
	
	BYTE	fileDescriptor;				// : 0x10, 0x20, 0x40, 0x80 1byte, 2byte, 4byte, 8byte.
										//<  fileDescriptor & 1 ==
	BYTE	unknown2[1];				//<  ???

	/*
	SHORT	versionNeededToExtract;
	_UGeneralPurposeBitFlag	generalPurposeBitFlag;
	SHORT	compressionMethod;
	SHORT	lastModFileTime;
	SHORT	lastModFileDate;
	UINT32	crc32;
	UINT32	compressedSize;
	UINT32	uncompressedSize;
	SHORT	fileNameLength;
	SHORT	extraFieldLength;
	*/
};

struct SAlzLocalFileHeader
{
	SAlzLocalFileHeader() { memset(this, 0, sizeof(*this)); }
	//~SAlzLocalFileHeader() { if(fileName) free(fileName); if(extraField) free(extraField); }
	void Clear() { if(fileName) free(fileName); fileName=NULL; if(extraField) free(extraField);extraField=NULL; }
	_SAlzLocalFileHeaderHead	head;

	BYTE					compressionMethod;			// : 2 - deflate, 1 - bzip2, 0 -
	BYTE					unknown;
	UINT32					fileCRC;					// CRC,

	INT64					compressedSize;
	INT64					uncompressedSize;

	CHAR*					fileName;
	BYTE*					extraField;
	INT64					dwFileDataPos;				//<  file data
	
	BYTE					encChk[ALZ_ENCR_HEADER_LEN];	// xf86
};

struct _SAlzCentralDirectoryStructureHead
{
	UINT32	dwUnknown;						// NULL
	UINT32	dwUnknown2;						// crc
	UINT32	dwCLZ03;						// "CLZ0x03" - 0x035a4c43
	/*
	SHORT	versionMadeBy;
	SHORT	versionNeededToExtract;
	_UGeneralPurposeBitFlag	generalPurposeBitFlag;
	SHORT	compressionMethod;
	SHORT	lastModFileTime;
	SHORT	lastModFileDate;
	UINT32	crc32;
	UINT32	compressedSize;
	UINT32	uncompressedSize;
	SHORT	fileNameLength;
	SHORT	extraFieldLength;
	SHORT	fileCommentLength;
	SHORT	diskNumberStart;
	SHORT	internalFileAttributes;
	UINT32	externalFileAttributes;
	UINT32	relativeOffsetOfLocalHeader;
	*/
};

struct SCentralDirectoryStructure
{
	SCentralDirectoryStructure() { memset(this, 0, sizeof(*this)); }
	//~SCentralDirectoryStructure() { if(fileName) free(fileName); if(extraField) free(extraField);if(fileComment)free(fileComment); }
	_SAlzCentralDirectoryStructureHead	head;
	/*
	CHAR*	fileName;
	BYTE*	extraField;
	CHAR*	fileComment;
	*/
};


/*
struct _SEndOfCentralDirectoryRecordHead
{
	SHORT	numberOfThisDisk;
	SHORT	numberOfTheDiskWithTheStartOfTheCentralDirectory;
	SHORT	centralDirectoryOnThisDisk;
	SHORT	totalNumberOfEntriesInTheCentralDirectoryOnThisDisk;
	UINT32	sizeOfTheCentralDirectory;
	UINT32	offsetOfStartOfCentralDirectoryWithREspectoTotheStartingDiskNumber;
	SHORT	zipFileCommentLength;
};
*/

/*
struct SEndOfCentralDirectoryRecord
{
	SEndOfCentralDirectoryRecord() { memset(this, 0, sizeof(*this)); }
	~SEndOfCentralDirectoryRecord() { if(fileComment) free(fileComment); }
	_SEndOfCentralDirectoryRecordHead head;
	CHAR*	fileComment;
};
*/

#ifdef _WIN32
#	pragma pack(pop, UNALZ)		//<  PACKING
#else
#	pragma pack()				// restore packing
#endif

//<  PROGRESS CALLBACK FUNCTION -
typedef void (_UnAlzCallback)(const char* szFileName, INT64 nCurrent, INT64 nRange, void* param, BOOL* bHalt);

class CUnAlz  
{
public:
	CUnAlz();
	~CUnAlz();
	BOOL	Open(const char* szPathName);
	void	Close();
	BOOL	SetCurrentFile(const char* szFileName);
	BOOL	ExtractCurrentFile(const char* szDestPathName, const char* szDestFileName=NULL);
	BOOL	ExtractCurrentFileToBuf(BYTE* pDestBuf, int nBufSize);		// pDestBuf=NULL
	BOOL	ExtractAll(const char* szDestPathName);
	void	SetCallback(_UnAlzCallback* pFunc, void* param=NULL);
	void	SetPipeMode(BOOL bPipeMode) {m_bPipeMode=bPipeMode;}

	void	SetPassword(char *passwd);  // xf86
	BOOL	chkValidPassword();			// xf86
	BOOL	IsEncrypted() { return m_bIsEncrypted; };

#ifdef _UNALZ_ICONV
	void	SetDestCodepage(const char* szToCodepage);
#endif

public :			//<  WIN32 ( UNICODE )

#ifdef _WIN32
#ifndef __GNUWIN32__
#ifndef LPCWSTR
	typedef const wchar_t* LPCWSTR;
#endif
	BOOL	Open(LPCWSTR szPathName);
	BOOL	SetCurrentFile(LPCWSTR szFileName);
	static BOOL		IsFolder(LPCWSTR szPathName);
#endif // __GNUWIN32__	
#endif // _WIN32

public :
	typedef vector<SAlzLocalFileHeader>		FileList;					//<
	FileList*			GetFileList() { return &m_fileList; };			//<  file
	void				SetCurrentFile(FileList::iterator newPos);		//< low level
	FileList::iterator	GetCurFileHeader() { return m_posCur; };		//< (SetCurrentFile() )

public :
	enum ERR							//<
	{
		ERR_NOERR,
		ERR_GENERAL,					//< GENERAL ERROR
		ERR_CANT_OPEN_FILE,				//< 
		ERR_CANT_OPEN_DEST_FILE,		//< 
//		ERR_CANT_CREATE_DEST_PATH,		//< 
		ERR_CORRUPTED_FILE,				//< 
		ERR_NOT_ALZ_FILE,				//< ALZ 
		ERR_CANT_READ_SIG,				//< signature 
		ERR_CANT_READ_FILE,

		ERR_AT_READ_HEADER,
		ERR_INVALID_FILENAME_LENGTH,
		ERR_INVALID_EXTRAFIELD_LENGTH,
		ERR_CANT_READ_CENTRAL_DIRECTORY_STRUCTURE_HEAD, 
		ERR_INVALID_FILENAME_SIZE,
		ERR_INVALID_EXTRAFIELD_SIZE,
		ERR_INVALID_FILECOMMENT_SIZE,
		ERR_CANT_READ_HEADER,
		ERR_MEM_ALLOC_FAILED,
		ERR_FILE_READ_ERROR,
		ERR_INFLATE_FAILED,
		ERR_BZIP2_FAILED,
		ERR_INVALID_FILE_CRC,	
		ERR_UNKNOWN_COMPRESSION_METHOD,

		ERR_ICONV_CANT_OPEN,
		ERR_ICONV_INVALID_MULTISEQUENCE_OF_CHARACTERS,
		ERR_ICONV_INCOMPLETE_MULTIBYTE_SEQUENCE,
		ERR_ICONV_NOT_ENOUGH_SPACE_OF_BUFFER_TO_CONVERT,
		ERR_ICONV_ETC,

		ERR_PASSWD_NOT_SET,
		ERR_INVALID_PASSWD,
		ERR_USER_ABORTED,

	};
	ERR		GetLastErr(){return m_nErr;}
	const char* GetLastErrStr(){return LastErrToStr(m_nErr);}
	const char* LastErrToStr(ERR nERR);

	enum SIGNATURE							//<  zip file signature - little endian
	{
		SIG_ERROR							= 0x00,
		SIG_EOF								= 0x01,
		SIG_ALZ_FILE_HEADER					= 0x015a4c41,	//<  ALZ 0x01
		SIG_LOCAL_FILE_HEADER				= 0x015a4c42,	//<  BLZ 0x01
		SIG_CENTRAL_DIRECTORY_STRUCTURE		= 0x015a4c43,	//<  CLZ 0x01
		SIG_ENDOF_CENTRAL_DIRECTORY_RECORD	= 0x025a4c43,	//<  CLZ 0x02
	};

public :
	static BOOL			DigPath(const CHAR* szPathName);
	static BOOL			IsFolder(const CHAR* szPathName);
	static const char*	GetVersion() { return UNALZ_VERSION; }
	static const char*	GetCopyright() { return UNALZ_COPYRIGHT; }
	BOOL				IsHalted() { return m_bHalt; }		// by xf86

public :
	static void			safe_strcpy(char* dst, const char* src, size_t dst_size);
	static void			safe_strcat(char* dst, const char* src, size_t dst_size);
	static unsigned int _strlcpy (char *dest, const char *src, unsigned int size);
	static unsigned int _strlcat (char *dest, const char *src, unsigned int size);

private :
	SIGNATURE	ReadSignature();
	BOOL		ReadAlzFileHeader();
	BOOL		ReadLocalFileheader();
	BOOL		ReadCentralDirectoryStructure();
	BOOL		ReadEndofCentralDirectoryRecord();

private :
	enum EXTRACT_TYPE				//<  
	{
		ET_FILE,					//<  FILE*
		ET_MEM,						//<  memory buffer
	};
	struct		SExtractDest		//<  
	{
		SExtractDest() { memset(this, 0, sizeof(SExtractDest)); }
		EXTRACT_TYPE nType;			//<  
		FILE*		fp;				//<  ET_FILE FILE*
		BYTE*		buf;			//<  ET_MEM
		UINT32		bufsize;		//<  ET_MEM
		UINT32		bufpos;			//<  ET_MEM
	};
	int			WriteToDest(SExtractDest* dest, BYTE* buf, int nSize);

private :
	BOOL		ExtractTo(SExtractDest* dest);

	//BOOL		ExtractDeflate(FILE* fp, SAlzLocalFileHeader& file);
	//BOOL		ExtractBzip2_bak(FILE* fp, SAlzLocalFileHeader& file); - 
	BOOL		ExtractDeflate2(SExtractDest* dest, SAlzLocalFileHeader& file);
	BOOL		ExtractBzip2(SExtractDest* dest, SAlzLocalFileHeader& file);
	BOOL		ExtractRawfile(SExtractDest* dest, SAlzLocalFileHeader& file);

private :		// bzip2
	typedef void MYBZFILE;
	MYBZFILE*	BZ2_bzReadOpen(int* bzerror, CUnAlz* f, int verbosity, int _small, void* unused, int nUnused);
	int			BZ2_bzread(MYBZFILE* b, void* buf, int len );
	int			BZ2_bzRead(int* bzerror, MYBZFILE* b, void* buf, int len);
	void		BZ2_bzReadClose( int *bzerror, MYBZFILE *b );

private :		// 
	BOOL		FOpen(const char* szPathName);
	void		FClose();
	INT64		FTell();
	BOOL		FEof();
	BOOL		FSeek(INT64 offset);
	BOOL		FRead(void* buffer, UINT32 nBytesToRead, int* pTotRead=NULL);

	BOOL		IsDataDescr() { return m_bIsDataDescr; };   // xf86
	int			getPasswordLen() { return strlen(m_szPasswd); };

	enum		{MAX_FILES=1000};								//< 
	enum		{MULTIVOL_TAIL_SIZE=16,MULTIVOL_HEAD_SIZE=8};	//< 
	struct SFile												//< 
	{
		HANDLE	fp;
		INT64	nFileSize;
		int		nMultivolHeaderSize;
		int		nMultivolTailSize;
	};

	SFile		m_files[MAX_FILES];					//< 
	int			m_nCurFile;							//< 
	int			m_nFileCount;						//< 
	INT64		m_nVirtualFilePos;					//< 
	INT64		m_nCurFilePos;						//< 
	BOOL		m_bIsEOF;							//< 

	BOOL		m_bIsEncrypted;						//< by xf86
	BOOL		m_bIsDataDescr;
#define UNALZ_LEN_PASSWORD	512
	char		m_szPasswd[UNALZ_LEN_PASSWORD];
	BOOL		m_bPipeMode;						//< pipemode - stdout

private :
	/*			from CZipArchive
	void		CryptDecodeBuffer(UINT32 uCount, CHAR *buf);
	void		CryptInitKeys();
	void		CryptUpdateKeys(CHAR c);
	BOOL		CryptCheck(CHAR *buf);
	CHAR		CryptDecryptCHAR();
	void		CryptDecode(CHAR &c);
	UINT32		CryptCRC32(UINT32 l, CHAR c);
	*/

private :		// encryption
	BOOL		IsEncryptedFile(BYTE fileDescriptor);
	BOOL		IsEncryptedFile();
	void		InitCryptKeys(const CHAR* szPassword);
	void		UpdateKeys(BYTE c);
	BOOL		CryptCheck(const BYTE* buf);
	BYTE		DecryptByte();
	void		DecryptingData(int nSize, BYTE* data);
	UINT32		CRC32(UINT32 l, BYTE c);
	UINT32		m_key[3];

private :
	FileList			m_fileList;					//< 
	ERR					m_nErr;
	FileList::iterator	m_posCur;					//< 
	_UnAlzCallback*		m_pFuncCallBack;
	void*				m_pCallbackParam;
	BOOL				m_bHalt;

#ifdef _UNALZ_ICONV

#define UNALZ_LEN_CODEPAGE	256
	char				m_szToCodepage[UNALZ_LEN_CODEPAGE];		//< codepage 
	char				m_szFromCodepage[UNALZ_LEN_CODEPAGE];		//< "CP949"
#endif
};
}

using namespace UNALZ;

#endif
