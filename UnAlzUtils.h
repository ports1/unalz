/*
  UNALZ : read and extract module for ALZ format.

  LICENSE (zlib License)
  Copyright (C) 2010-2018 portmasterATbsdforge.com : https://BSDforge.com
  Copyright (C) 2004-2009 kippler@gmail.com

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  =============================================================================================================
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// unalz
// 
// @author       kippler@gmail.com
// @date         2005-06-23 9:55:34
// @updated-by   portmasterATbsdforge.com
// @last-update  2018-02-24
// 
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _UNALZ_UTILS_H_
#define _UNALZ_UTILS_H_

#include "UnAlz.h"

time_t	dosTime2TimeT(UINT32 dostime);
int		ListAlz(CUnAlz* pUnAlz, const char* src);

#endif // _UNALZ_UTILS_H_
